/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    loaderFile: './src/helpers/image-loader.ts',
  },
};

module.exports = nextConfig;
