import { Item } from '~/types/app';

export const joinToStringFromArray = (array: Item[] = []) => {
  return array.map((item) => item.name).join(', ');
};
