export const APP_CONFIG = {
  NAME: 'Phim Plus',
  DESCRIPTION:
    'PhimPlus - Trang xem phim Online với giao diện mới được bố trí và thiết kế thân thiện với người dùng. Nguồn phim được tổng hợp từ các website lớn với đa dạng các đầu phim và thể loại vô cùng phong phú.',
};

export const APP_ROUTERS = {
  HOME: '/',
  LIST: {
    FILM: '/danh-sach/phim-le',
    SERIES: '/danh-sach/phim-bo',
    ANIME: '/danh-sach/phim-hoat-hinh',
    UPCOMMING: '/danh-sach/phim-sap-chieu',
  },
  CATEGORY: '/the-loai/',
  MOVIE: '/phim/',
  WATCH: '/xem-phim/',
};

export const MENU_BAR = [
  {
    id: 1,
    title: 'Phim lẻ',
    path: APP_ROUTERS.LIST.FILM,
    key: 'film',
    active: 'phim-le',
  },
  {
    id: 2,
    title: 'Phim bộ',
    path: APP_ROUTERS.LIST.SERIES,
    key: 'series',
    active: 'phim-bo',
  },
  {
    id: 3,
    title: 'Phim hoạt hình',
    path: APP_ROUTERS.LIST.ANIME,
    key: 'anime',
    active: 'phim-hoat-hinh',
  },
  {
    id: 4,
    title: 'Thể loại',
    path: '',
    key: 'categories',
    active: 'the-loai',
  },
  {
    id: 5,
    title: 'Quốc gia',
    path: '',
    key: 'countries',
    active: 'quoc-gia',
  },
];

export const MENU_INFORMATION = [
  { id: 1, name: 'Giới Thiệu', slug: '' },
  { id: 1, name: 'Liên Hệ Chúng Tôi', slug: '' },
  { id: 1, name: 'Điều Khoản Sử Dụng', slug: '' },
  { id: 1, name: 'Chính Sách Riêng Tư', slug: '' },
  { id: 1, name: 'Kiếu Nại Bản Quyền', slug: '' },
];
