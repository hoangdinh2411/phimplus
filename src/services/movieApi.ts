import request from './request';
import { IMovieWithSeo, IListMovieWithSeo } from '~/types/movie';

export const fetchListNewMovie = async (year?: number) => {
  const res = await request<IListMovieWithSeo>(
    `/v1/api/danh-sach/phim-moi?year=${year}`,
    {
      next: { revalidate: 3600 },
    }
  );

  return res.data;
};

export const getMovieBySlug = async (slug: string) => {
  return await request<IMovieWithSeo>('/v1/api/phim/' + slug);
};
