import Container from '@mui/material/Container';
import React from 'react';
import { Metadata, ResolvingMetadata } from 'next';
import dynamic from 'next/dynamic';
import Grid from '@mui/material/Grid';
import MovieSlide from '~/components/shared/slide/MovieSlide';
const Cart = dynamic(() => import('~/components/UI/Card'), { ssr: false });
import { fetchListNewMovie } from '~/services/movieApi';
import AdsSlide from './_pages/home/AdsSlide/AdsSlide';

export async function generateMetadata(
  parent: ResolvingMetadata
): Promise<Metadata> {
  const newMovies = await fetchListNewMovie(new Date().getFullYear());
  const previousImages = (await parent).openGraph?.images || [];
  return {
    title: newMovies.seoOnPage.titleHead || newMovies.titlePage,
    description: newMovies.seoOnPage?.descriptionHead,
    openGraph: {
      title: newMovies.titlePage,
      description: newMovies.seoOnPage?.descriptionHead,
      images: [...newMovies.seoOnPage.og_image, ...previousImages],
    },
  };
}

export default async function Home() {
  const newMovies = await fetchListNewMovie(new Date().getFullYear());

  return (
    <>
      <AdsSlide items={newMovies.items} />
      <MovieSlide items={newMovies?.items} />
      <Container
        component='main'
        maxWidth={false}
        disableGutters
        sx={{
          py: 30,
          px: {
            xs: 8,
            lg: 0,
          },
        }}
      >
        <Grid spacing={8} container>
          {Array(10)
            .fill('')
            .map((item, index) => {
              return (
                <Grid key={index} item md={3} sm={4} xs={6}>
                  <Cart
                    name='phim abc '
                    slug='phim-abc'
                    thumbnail='tan-olympus-thumb.jpg'
                    tagTitle='Phim hai'
                  />
                </Grid>
              );
            })}
        </Grid>
      </Container>
    </>
  );
}
