'use client';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';
import ThumbDownOutlinedIcon from '@mui/icons-material/ThumbDownOutlined';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import Button from '@mui/material/Button';
import useMovie from '~/hooks/useMovie';
import Link from '@mui/material/Link';
import NextLink from 'next/link';
import { APP_ROUTERS } from '~/helpers/config';
import React from 'react';
export default function Info() {
  const { movie } = useMovie();

  const getMovieStatus = React.useCallback(() => {
    switch (movie.item.episode_current) {
      case 'Full':
        return 'Full';
      case 'Trailer':
        return 'Trailer';

      default:
        return movie.item.episode_current + '/' + movie.item.episode_total;
    }
  }, [movie.item.episode_current, movie.item.episode_total]);
  return (
    <Box
      component='aside'
      sx={{
        my: {
          md: 0,
          xs: 20,
        },
        pl: {
          xs: 0,
          md: 20,
        },
        width: {
          xs: 400,
          md: '100%',
        },
        height: {
          lg: 450,
          xs: '100%',
        },
        marginX: 'auto',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Typography pb={12} variant='h4' component='h4' fontWeight='600'>
        {movie.item.name}
      </Typography>

      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Tên gốc:
        <Typography
          variant='caption'
          component='span'
          color='primary.dark'
          ml={4}
        >
          {movie.item.origin_name}
        </Typography>
      </Typography>
      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Thời lượng:
        <Typography
          variant='caption'
          component='span'
          color='primary.dark'
          ml={4}
        >
          {movie.item.time}
        </Typography>
      </Typography>
      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Mới nhất:
        <Typography
          variant='caption'
          component='span'
          color='primary.dark'
          ml={4}
        >
          {getMovieStatus()}
        </Typography>
      </Typography>
      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Năm sản xuất:
        <Typography
          variant='caption'
          component='span'
          color='primary.dark'
          ml={4}
        >
          {movie.item.year}
        </Typography>
      </Typography>
      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Thể loại:
        <>
          {movie.item.category.map((item) => (
            <Typography key={item.id} variant='caption' component='span' ml={4}>
              <Link
                href={APP_ROUTERS.CATEGORY + item.slug}
                component={NextLink}
                prefetch={false}
                color='primary.dark'
                sx={{
                  textDecoration: 'underline',
                }}
              >
                {item.name}
              </Link>
            </Typography>
          ))}
        </>
      </Typography>
      <Typography
        pb={8}
        variant='h6'
        component='p'
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Đạo diễn:
        <Typography
          variant='caption'
          component='span'
          color='primary.dark'
          ml={4}
        >
          {movie.item.director.join('- ')}
        </Typography>
      </Typography>

      <Box
        pb={8}
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Typography
          variant='caption'
          component='span'
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <ThumbUpOutlinedIcon color='success' sx={{ mr: 2, fontSize: 18 }} />
          {movie.item.view}
        </Typography>
        <Typography
          variant='caption'
          component='span'
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <ThumbDownOutlinedIcon
            color='error'
            sx={{ ml: 8, mr: 2, fontSize: 18 }}
          />
          0
        </Typography>
      </Box>

      <Box width={200} mt={8}>
        <Button variant='contained' size='large'>
          <Link
            href={APP_ROUTERS.WATCH + movie.item.slug}
            component={NextLink}
            prefetch={false}
            sx={{
              textTransform: 'uppercase',
              fontWeight: 'bold',
              display: {
                md: 'flex',
                xs: 'none',
              },
              alignItems: 'center',
            }}
          >
            <PlayArrowIcon />
            Xem phim{' '}
          </Link>
        </Button>
      </Box>
    </Box>
  );
}
