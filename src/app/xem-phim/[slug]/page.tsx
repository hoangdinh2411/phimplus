import Container from '@mui/material/Container';
import { Metadata } from 'next';
import React from 'react';

export const metadata: Metadata = {
  title: 'xem phim',
};

export default function Page() {
  return (
    <Container
      component='main'
      maxWidth={false}
      disableGutters
      sx={{
        py: 30,
        px: {
          xs: 8,
          lg: 0,
        },
      }}
    >
      Xem phim
    </Container>
  );
}
