import React from 'react';
import List from '@mui/material/List';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarOutlinedIcon from '@mui/icons-material/StarOutlined';
import ListItem from '@mui/material/ListItem';
import SvgIcon from '@mui/material/SvgIcon';
const Stars = ({ point }: { point: number }) => {
  const amountOfStars = 5;
  const stars = Array(amountOfStars)
    .fill('')
    .map((_, index) => {
      const isFilled = index + 1 <= point;
      return (
        <SvgIcon
          key={index}
          color='primary'
          sx={{
            fontSize: {
              lg: 20,
              xs: 14,
            },
          }}
        >
          {isFilled ? <StarOutlinedIcon /> : <StarBorderIcon />}
        </SvgIcon>
      );
    });
  return (
    <List
      sx={{
        display: 'inline-flex',
        alignItems: 'center',
        my: {
          lg: 8,
          xs: 2,
        },
        padding: 0,
        color: 'text.primary',
      }}
    >
      {stars.map((item, index) => {
        return (
          <ListItem key={index} disablePadding>
            {item}
          </ListItem>
        );
      })}
    </List>
  );
};

export default React.memo(Stars);
